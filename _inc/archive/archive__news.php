<section class="archiveNewsPageSec">
    <div class="archiveNewsPageSec__innerArea">
        <h1 class="archiveNewsPageSec__title">NEWS</h1>

        <?php if ( have_posts() ) : ?>
            <ul class="postListA">
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php
                    $newsPost_thumb = get_field('newsPost_thumb');
                    $newsPost_thumb_url = $newsPost_thumb['url'];
                    $newsPost_thumb_title = $newsPost_thumb['title'];
                    ?>
                    <li class="postListA__item">
                        <a href="<?php echo get_permalink(); ?>" class="postListA__link">
                            <div class="postListA__thumbArea" style="background-image: url('<?php echo $newsPost_thumb_url; ?>');"><?php echo $newsPost_thumb_title; ?></div>
                            <div class="postListA__textArea">
                                <time class="postListA__time"><?php echo get_the_time('Y.m.d'); ?></time>
                                <p class="postListA__postTitle"><?php echo get_the_title(); ?></p>
                            </div>
                        </a>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</section>
