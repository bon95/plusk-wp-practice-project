const smoothScroll = () => {
  $('a[href^="#"]').on('click', e => {
    const speed = 600;
    const href = $(this).attr('href');
    const target = $(href == '#' || href == '' ? 'html' : href);
    const position = target.offset().top;
    $('body, html').delay(200).animate({scrollTop: position}, speed, 'swing');
    return false;
  });
};

const fancyBoxModal = () => {
  $('[data-fancybox]').fancybox({
    loop: true, // 複数画像表示時に最初と最後をループさせる
    transitionEffect: 'slide', // スライド時のアニメーション効果の種類
    transitionDuration: 1000, // スライド時のアニメーションにかかる時間
    animationEffect: 'fade', // 画像ズーム時のアニメーションの種類
    animationDuration: 800, // ズーム時のアニメーションにかかる時間
    infobar: true, // 上部にインフォメーションバーを表示するか否か
  });
}

const btnClickFunc = () => {
  const $btns = $('.js-btn');

  $btns.on('click', e => {
    const btnTargetData = $(e.currentTarget).attr('data-btn');
    const btnAnimationData = $(e.currentTarget).attr('data-animation');
    const target = $(`[data-target = "${btnTargetData}"]`);
    const btnTargetGroupData = $(e.currentTarget).attr('data-btnGroup');
    const $sameBtns = $(`[data-btnGroup = "${btnTargetGroupData}"]`);
    const $sameTargets = $(`[data-targetGroup = "${btnTargetGroupData}"]`);

    if (btnAnimationData == 'slide') {
      $(e.currentTarget).toggleClass('is-open');
      target.slideToggle(300);
    } else if (btnAnimationData == 'class') {
      $(e.currentTarget).toggleClass('is-active');
      target.toggleClass('is-active');
    } else if (btnAnimationData == 'commonFade') {
      $sameBtns.toggleClass('is-active');
      target.fadeToggle(300);
    } else if (btnAnimationData == 'allClass') {
      $(e.currentTarget).toggleClass('is-active');
      $sameTargets.addClass('is-active');
    } else if (btnAnimationData == 'tab') {
      $sameBtns.not($(e.currentTarget)).removeClass('is-open');
      $(e.currentTarget).addClass('is-open');
      $sameTargets.removeClass('is-open');
      target.addClass('is-open');
    } else if (btnAnimationData == 'pageTop') {
      $('body, html').animate({
        scrollTop: 0
      }, 700);
      return false;
    } else {
      $(e.currentTarget).toggleClass('is-active');
    }
  });
};

$(function() {
  smoothScroll();
  fancyBoxModal();
  btnClickFunc();
});
